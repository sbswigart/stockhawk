package com.sam_chordas.android.stockhawk.widget;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.os.Binder;
import android.os.Bundle;
import android.widget.RemoteViews;
import android.widget.RemoteViewsService;

import com.sam_chordas.android.stockhawk.R;
import com.sam_chordas.android.stockhawk.data.QuoteColumns;
import com.sam_chordas.android.stockhawk.data.QuoteProvider;

/**
 * Created by ss on 4/25/16.
 */
public class StockHawkWidgetService extends RemoteViewsService {

    @Override
    public RemoteViewsFactory onGetViewFactory(Intent intent) {
        return new StockHawkRemoteViewsFactory(this.getApplicationContext());
    }

    class StockHawkRemoteViewsFactory implements RemoteViewsService.RemoteViewsFactory {
        private Context mContext;
        private Cursor mCursor;

        public StockHawkRemoteViewsFactory(Context context) {
            mContext = context;
        }

        public void onCreate() {

            mCursor = getContentResolver().query(QuoteProvider.Quotes.CONTENT_URI,
                    new String[] { "Distinct " + QuoteColumns.SYMBOL, QuoteColumns.BIDPRICE, QuoteColumns.PERCENT_CHANGE, QuoteColumns.ISUP }, null,
                    null, null);

            DatabaseUtils.dumpCursor(mCursor);
        }

        public RemoteViews getViewAt(int position) {

            mCursor.moveToPosition(position);

            RemoteViews rv = new RemoteViews(mContext.getPackageName(), R.layout.list_item_quote);
            String symbol = mCursor.getString(mCursor.getColumnIndex(QuoteColumns.SYMBOL));
            String bid = mCursor.getString(mCursor.getColumnIndex(QuoteColumns.BIDPRICE));
            String change = mCursor.getString(mCursor.getColumnIndex(QuoteColumns.PERCENT_CHANGE));

            rv.setTextViewText(R.id.stock_symbol, symbol);
            rv.setTextViewText(R.id.bid_price, bid);
            rv.setTextViewText(R.id.change, change);

            if (mCursor.getInt(mCursor.getColumnIndex("is_up")) == 1){
                rv.setTextColor(R.id.change, getResources().getColor(R.color.material_green_700 ));
            } else{
                rv.setTextColor(R.id.change, getResources().getColor(R.color.material_red_700 ));
            }

            Bundle extras = new Bundle();
            extras.putInt(StockHawkWidgetProvider.EXTRA_ITEM, position);
            Intent fillInIntent = new Intent();
            fillInIntent.putExtras(extras);

            rv.setOnClickFillInIntent(R.id.widget_list_view, fillInIntent);

            return rv;
        }

        @Override
        public int getCount() {
            return mCursor.getCount();
        }

        @Override
        public void onDataSetChanged() {
            if (mCursor != null) {
                mCursor.close();
            }
            final long identityToken = Binder.clearCallingIdentity();
            mCursor = getContentResolver().query(QuoteProvider.Quotes.CONTENT_URI,
                    new String[] { "Distinct " + QuoteColumns.SYMBOL, QuoteColumns.BIDPRICE, QuoteColumns.PERCENT_CHANGE, QuoteColumns.ISUP }, null,
                    null, null);
            Binder.restoreCallingIdentity(identityToken);
        }

        @Override
        public int getViewTypeCount() {
            return 1;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public void onDestroy() {
            if (mCursor != null) {
                mCursor.close();
                mCursor = null;
            }

        }

        @Override
        public RemoteViews getLoadingView() {
            return null;
        }

        @Override
        public boolean hasStableIds() {
            return false;
        }
    }
}


package com.sam_chordas.android.stockhawk.rest;

/**
 * Created by ss on 4/23/16.
 */
public class Graph {

    private String[] mLabels;
    private float[][] mValues;
    private String mSymbol;
    private int mMax;
    private int mMin;

    public String[] getLabels() {
        return mLabels;
    }

    public void setLabels(String[] label) {
        mLabels = label;
    }

    public float[][] getValues() {
        return mValues;
    }

    public void setValues(float[][] values) {
        mValues = values;
    }

    public String getSymbol() {
        return mSymbol;
    }

    public void setSymbol(String symbol) {
        mSymbol = symbol;
    }

    public int getMax() {
        return mMax;
    }

    public void setMax(int max) {
        mMax = max;
    }

    public int getMin() {
        return mMin;
    }

    public void setMin(int min) {
        mMin = min;
    }
}

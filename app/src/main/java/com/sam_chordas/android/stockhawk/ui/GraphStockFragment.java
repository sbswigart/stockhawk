package com.sam_chordas.android.stockhawk.ui;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.db.chart.model.LineSet;
import com.db.chart.view.LineChartView;
import com.sam_chordas.android.stockhawk.R;
import com.sam_chordas.android.stockhawk.rest.Graph;
import com.sam_chordas.android.stockhawk.rest.GraphFetcher;

/**
 * Created by ss on 4/20/16.
 */
public class GraphStockFragment extends Fragment {

    private LineChartView mChart;
    private Graph mGraph;

    private String[] mLabels;
    private float[][] mValues;
    private String mStockName;
    private static String STOCK_NAME = "stock_name";

    public static GraphStockFragment newInstance(String stockName) {
         Bundle args = new Bundle();
        args.putSerializable(STOCK_NAME, stockName);

        GraphStockFragment fragment = new GraphStockFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mStockName = (String) getArguments().getSerializable(STOCK_NAME);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View graphView = inflater.inflate(R.layout.activity_line_graph, container, false);
        mChart = (LineChartView) graphView.findViewById(R.id.linechart);
        new FetchGraph().execute();
        return graphView;
    }

    private void setUpChart() {
        // Data

        LineSet dataset = new LineSet(mLabels, mValues[0]);
        dataset.setColor(Color.parseColor("#b3b5bb"))
                .setFill(Color.parseColor("#2d374c"))
                .setDotsColor(Color.parseColor("#ffc755"))
                .setThickness(4)
                .endAt(mLabels.length);
        mChart.addData(dataset);

//        // Chart
//        mChart.setBorderSpacing(Tools.fromDpToPx(15))
////                .setAxisBorderValues(0, 20)
//                .setYLabels(AxisController.LabelPosition.NONE)
//                .setLabelsColor(Color.parseColor("#6a84c3"))
//                .setXAxis(false)
//                .setYAxis(false);

        mChart.setAxisBorderValues(mGraph.getMin(), mGraph.getMax());

        mChart.show();

    }

    private void setUpData() {
        mLabels = mGraph.getLabels();
        mValues = mGraph.getValues();
        setUpChart();
    }

    private class FetchGraph extends AsyncTask<Void, Void, Graph> {

        @Override
        protected Graph doInBackground(Void... params) {
            return new GraphFetcher().fetchGraph(mStockName);
        }

        @Override
        protected void onPostExecute(Graph graph) {
            mGraph = graph;
            setUpData();
        }
    }
}

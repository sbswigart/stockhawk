package com.sam_chordas.android.stockhawk.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.sam_chordas.android.stockhawk.R;

/**
 * Created by ss on 4/17/16.
 */
public class GraphStockActivity extends AppCompatActivity {

    private static String STOCK_NAME = "stock_name";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fragment);

        String stockName = (String) getIntent().getSerializableExtra(STOCK_NAME);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.frame_container, GraphStockFragment.newInstance(stockName)).commit();
        }
    }
}

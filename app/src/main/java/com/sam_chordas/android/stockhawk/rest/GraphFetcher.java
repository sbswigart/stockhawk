package com.sam_chordas.android.stockhawk.rest;

import android.util.Log;

import com.squareup.okhttp.OkHttpClient;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by ss on 4/22/16.
 */

public class GraphFetcher {

    private final String TAG = GraphFetcher.class.getSimpleName();

    private static String GRAPH_API_BASE = "https://query.yahooapis.com/v1/public/yql?q=";
    private OkHttpClient client = new OkHttpClient();

    public Graph fetchGraph(String stock) {
        Graph graph = new Graph();
        Date date = new Date();
        String todaysDate = new SimpleDateFormat("yyyy-MM-dd").format(date);
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -7);
        Date previousDate = cal.getTime();
        String weekAgoDate = new SimpleDateFormat("yyyy-MM-dd").format(previousDate);

        StringBuilder urlStringBuilder = new StringBuilder();
        graph.setSymbol(stock);
        try{
            // Base URL for the Yahoo query
            urlStringBuilder.append(GRAPH_API_BASE);
            urlStringBuilder.append(URLEncoder.encode("select * from yahoo.finance.historicaldata where symbol = \"" + stock
                    + "\" and startDate = \"" + weekAgoDate + "\" and endDate= \"" + todaysDate + "\"", "UTF-8"));
            urlStringBuilder.append("&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables."
                    + "org%2Falltableswithkeys&callback=");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        String urlString = urlStringBuilder.toString();
        Log.i(TAG, "QUERY in GraphFetcher: " + urlString);
        try {
            String json = Utils.fetchData(urlString, client);
            Log.i(TAG, "RESPONSE in GraphFetcher: " + json);
            parseGraph(json, graph);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } catch (JSONException je) {
            je.printStackTrace();
        }

        return graph;
    }

    private void parseGraph(String jsonString, Graph graph) throws JSONException {
        JSONObject jsonBody = new JSONObject(jsonString);
        jsonBody = jsonBody.getJSONObject("query");
        jsonBody = jsonBody.getJSONObject("results");
        JSONArray jsonArray = jsonBody.getJSONArray("quote");
        int length = jsonArray.length();
        String[] labels = new String[length];
        float[][] values = new float[1][length];
        float firstDataValue = (float) jsonArray.getJSONObject(0).getDouble("Close");
        float max = firstDataValue;
        float min = firstDataValue;

        for (int i = 0; i < length; i++) {
            JSONObject dataPointObject = jsonArray.getJSONObject(i);
            float value = (float) dataPointObject.getDouble("Close");
            String label = dataPointObject.getString("Date").substring(5);

            values[0][length - (i + 1)] = value;
            labels[length - (i + 1 )] = label;

            if (value > max) {
                max = value;
                continue;
            }

            if (value < min) {
                min = value;
            }
        }

        graph.setLabels(labels);
        graph.setValues(values);
        graph.setMax(Math.round(max) + 2);
        graph.setMin(Math.round(min) - 2);
    }
}
